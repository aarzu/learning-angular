import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importas componentes
import { EmpleadoComponent } from './empleados/empleados.component';
import { FrutaComponent } from './fruta/fruta.component';
import { HomeComponent } from './home/home.component'
import { ContactoComponent } from './contacto/contacto.component'

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'empleados', component: EmpleadoComponent },
    { path: 'frutas', component: FrutaComponent },
    { path: 'contacto', component: ContactoComponent },
    { path: 'contacto/:page', component: ContactoComponent },
    { path: 'home', component: HomeComponent },
    { path: '**', component:HomeComponent }
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);