import { Component } from '@angular/core';

@Component({
   selector: 'fruta',
   templateUrl: './frua.component.html'
})

export class FrutaComponent{
   public nombre_componente = 'Componente de fruta';
   public listado_frutas = 'Naranja, Manzana, Pera y Sandia';
   public trabajos: Array<any> = ['Carpintero', 7, 'Ingeniero'];

   constructor(){
   }
   
   ngOnInit(){
      this.cambiarTrabajos();
      // variables y alcance
      var uno = 1;
      var dos = 2;

      if (uno == 1) {
         let uno = 3; // solo tiene alcance dentro del if
         var dos = 33; // tiene alcance fuera del if
         console.log("Dentro del 1")
      }
   }

   cambiarTrabajos(){
      this.trabajos = ['Desarrollador', 'Ingeniero', 'Diseñador'];
   }
}