import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'contacto',
    templateUrl: './contacto.Component.html'
})
export class ContactoComponent{
    public titulo = "Página de contacto";
    public parametro: any;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router
    ){}

    ngOnInit(){
        this._route.params.forEach((params: Params) => {
            this.parametro = params['page'];
        })
    }

    redirigir(){
        this._router.navigate(['/contacto','Ariel Araneda Zúñiga']);
    }
}