import { Component } from '@angular/core';

@Component({
    selector: 'home',
    templateUrl: './home.Component.html'
})
export class HomeComponent{
    public titulo = "Página principal";
}