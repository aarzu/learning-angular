import { Component } from '@angular/core';
import { Empleado } from './empleados'

@Component({
   selector: 'empleados',
   templateUrl: './empleados.component.html'
})

export class EmpleadoComponent{
   public nombre_componente = 'Componente Empleados';
   public nombre_empleado = 'Ariel Araneda';
   public empleado: Empleado;
   public trabajadores: Array<Empleado>;
   public color:string;
   public color_cuadro:string;

   constructor(){
      this.empleado = new Empleado('Patricio Araneda', 45, 'Ingeniero', true);
      this.trabajadores = [
         new Empleado('Patricio Araneda', 46, 'Ingeniero', true),
         new Empleado('Vivianne Zúñiga', 45, 'Ingeniero', true),
         new Empleado('Felipe Araneda', 21, 'Ingeniero', true)
      ];

      this.color = 'green';
   }

   ngOnInit() {
      console.log(this.trabajadores)
   }

   cambiarEstado(){
      if (this.trabajadores[1].contratado == true){
         this.trabajadores[1].contratado = false;
      } else {
         this.trabajadores[1].contratado = true;
      }
   }
}